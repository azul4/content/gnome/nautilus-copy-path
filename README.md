# nautilus-copy-path

Configurable extension for Nautilus to copy path, URI, or name

https://github.com/chr314/nautilus-copy-path

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/nautilus-copy-path.git
```

